const fs = require('fs');
const { spawnSync } = require('child_process');
const internalDataModel = require('./middlewares/internalDataModel');

// Full HD size
let w = "1920"
let h = "1080"
let size = `${w}x${h}`
const export_options = {
  "w":w,
  "h":h,
  "size":size
}

// Peertube Miniatures size
let w_th = "280"
let h_th = "150"
let size_th = `${w_th}x${h_th}`
const export_options_th = {
  "w":w_th,
  "h":h_th,
  "size":size_th
}

// Create directories to contain files generation
async function createBaseDirs(){
  const pathsToCheck = ['./output', './output/samedi', './output/dimanche'];
  for (let path of pathsToCheck){
    await createPath(path);
  }
}

// Create directory for each conference/atelier
async function createPath(path){
  return new Promise((resolve, reject) =>{
    try {
      fs.stat(path, (err, stats) => {
        if (typeof stats == 'undefined'){
          console.log(`${path} created`)
          return resolve(fs.mkdirSync(path, {recursive:true}));
        } else {
          console.log('exists');
          return resolve();
        }
      });
    } catch(err){
      reject(err)
    }
  })
}

// Replace $NAME placaholder in filler templates
// by name of the speakers
// TODO fix to get one filler by speaker
function replaceInFiller(src, dest, name){
  let data = fs.readFileSync(src, 'utf8')
  let result = data.replace(/\$NAME/g, name);
  fs.writeFileSync(dest, result, 'utf8')
}

// Replace $SPEAKERS and $TITLE in templates
// by title of conference and names of speakers
function replaceInTemplate(src, dest, options){
  let data = fs.readFileSync(src, 'utf8')
  let result = data.replace(/\$START/g, options.start)
    .replace(/\$SPEAKERS/g, options.speakers)
    .replace(/\$TITLE/g, options.title);
  fs.writeFileSync(dest, result, 'utf8')
}

// Convert svg files to png files
function svgToPng(src, dest, options){
  let params = new Array
  if (options?.w){
    params.push("-w", options.w)
  }
  if (options?.h){
    params.push("-h", options.h)
  }
  params.push(src, "-o", `${dest}`)
  console.log(params)
  spawnSync('inkscape', params)
}

// Generate Left and Right fillers with speakers names   
// TODO fix to get fillers with only one speaker name
function makeFillers(event, folder){
  let srcs = ['L', 'R']
  let escapedName = event.speakers.replace("/","_")
  let base = `${folder}/${escapedName}`
  for (let src of srcs){
    let svg = `${base}_${src}.svg`
    let png = `${base}_${src}.png`
    replaceInFiller(`./input/bandeau_${src}.svg`, svg, event.speakers)
    svgToPng(svg, png, export_options)
  }
}

// Generic function to generate svg and png from template and data
function makeGraphicFiles(event, folder, template, exp_options=export_options){
  let startTime = new Date(event.startTime)
  let start = `${startTime.getHours()}h${startTime.getMinutes()!=0?startTime.getMinutes():""}`
  let prefix =  exp_options === export_options ? "" : "Miniature_"
  let base = `${folder}/${prefix}${template}_${start}`
  let options = {
    "start":start,
    "speakers":event.speakers,
    "title":event.title
  }
  let svg = `${base}.svg`
  let png = `${base}.png`
  replaceInTemplate(`./input/${template}.svg`, svg, options)
  svgToPng(svg, png, exp_options)
}

// Generation of all layouts for a particular conf
async function makeLayouts(event, folder){
  const templates=["next", "title", "pip"]

  for (let template of templates){
    makeGraphicFiles(event, folder, template)
  }
  // make miniature
  makeGraphicFiles(event, folder, title, export_options_th)
  makeFillers(event, folder)
}

// Generate all layouts for an event
async function generateLayouts(){
  const days = ['samedi', 'dimanche'];
  const types = ['confs', 'ateliers', 'gones'];
  let data = await internalDataModel();
  for (let day of days){
    for (let type of types){
      for (let event of data[day][type]){
        let start = new Date(event.startTime)
        let path = `./output/${day}/${event.room}`
        await createPath(path);
        let subpath = `./output/${day}/${event.room}/${start.getHours()}h${start.getMinutes()!=0?start.getMinutes():""}`
        await createPath(subpath);
        await makeLayouts(event, subpath);
      }
    }
  }
  return data;
}

let main = async ()=>{
  try{
    await createBaseDirs().catch((err)=>{console.error("Path creation:", err)});
    await generateLayouts().catch((err)=>{console.error("Layout generation:", err)});
  } catch (err){
    console.error('Error:',err);
  }
}

main();
